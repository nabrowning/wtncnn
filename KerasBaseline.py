import numpy as np
import h5py
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.optimizers import SGD
import os

def createDense():
    model = Sequential()
    model.add(Dense(200, input_shape=(784,)))
    model.add(Activation('sigmoid'))
    model.add(Dense(10))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy',
              optimizer=SGD(lr=0.9, momentum=0.0, nesterov=False, decay=0.0),
              metrics=['accuracy'])
    return model

def createConv():
    model = Sequential()

    # First convolutional layer
    # model.add(Convolution2D(32, 5, 5, border_mode='valid', input_shape=(32, 1, 28,28)))
    model.add(Convolution2D(32, 5, 5, border_mode='valid', input_shape=(28,28,1)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #
    # Second convolutional layer
    model.add(Convolution2D(64, 5, 5))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(1024))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))

    model.add(Dense(10))
    model.add(Activation('softmax'))

    #sgd = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss='categorical_crossentropy', optimizer="adam", metrics=['accuracy'])

    return model

def getTrainTest(X, y):
    perm = np.random.permutation(range(len(X)))
    X = X[perm]
    y = y[perm]
    X_train, X_test = X[:len(X)/2], X[len(X)/2:]
    y_train, y_test = y[:len(y)/2], y[len(y)/2:]
    return X_train, X_test, y_train, y_test

def loadDataset1D():
    X1d, y1d = loadMNIST()
    return getTrainTest(X1d, y1d)

def loadDataset2D():
    X2d, y2d = loadMNIST2D()
    return getTrainTest(X2d, y2d)

def loadMNIST():
    size = 60000
    os.chdir("/Users/Winston/PycharmProjects/ImgProcessing")
    f = h5py.File("mnist.hdf5", 'r')
    X = f['x_train'][:size]

    maxes = X.max(axis=0)
    for i in range(len(maxes)):
        if maxes[i] == 0:
            maxes[i] = 0.1
    X *= 1/maxes
    # print X.shape

    raw_y = np.array([f['t_train'][:size]]).T

    y = []
    for row in raw_y:
        y.append(convertToOneHot(row[0], 10))

    y = np.array(y)

    print "MNIST Dataset LOADED"

    return X, y

def loadMNIST2D():
    X, y = loadMNIST()
    # Convert the X into 2-D images

    width = 28
    height = 28

    number_samples = len(X)
    # print X.shape
    # X = np.reshape(X, (number_samples, 1, width, height))
    # print X.shape, y.shape
    X = np.reshape(X, (number_samples, width, height, 1))
    print "MNIST2D Dataset LOADED"

    return X, y

def convertToOneHot(val, size):
    x = np.zeros(size)
    x[val] = 0.9
    return x

def saveModel(model):
    model.save("MNISTTest.hdf5")

def loadModel(filename):
    model = loadModel(filename)
    return model


if __name__ == "__main__":
    X_train, X_test, y_train, y_test = loadDataset1D()
    model = createDense()

    # X_train, X_test, y_train, y_test = loadDataset2D()
    # model = createConv()

    minibatch_size = 32

    model.fit(X_train, y_train,
              batch_size=minibatch_size,
              nb_epoch=100,
              validation_data=(X_test, y_test),
              verbose=1)
